+++
abstract = "During the European Refugee Situation in 2015, Sweden received an influx of forcibly displaced persons seeking asylum, the highest number of asylum seekers per capita among European Union member states. A challenge existed in providing accommodations within the Swedish built environment, in contrast to other housing solutions such as geographically distinct refugee camps. Regulations are one method of maintaining consistent standards of buildings, but they are effective only if they are enforced. During uncertainty, compliance decisions can be based on cognitive understanding (cognitive legitimacy). Otherwise, deterrence-based regulation is rooted in direct consequences (pragmatic legitimacy), and responsive regulation relies on normative motivation (moral legitimacy) for compliance. This study explored the normative aspect of regulatory enforcement through inspections. In 2017, 34 semistructured interviews were conducted with individuals involved with providing temporary accommodations in Sweden during 2015–2016. From these, six interviews with employees of regulatory enforcement agencies were qualitatively analyzed to discover how regulators legitimized. Findings show that regulators justified their inspections predominantly based on what is considered as a socially acceptable approach (procedural legitimacy) and what is understandable based on their personal experience (comprehensibility legitimacy). This study provides practical insight to regulators regarding the benefits for using responsive regulation during institutional responses to mass population displacement in urban communities. Findings will assist in ensuring that the quality of buildings is consistent for the general population and reduces variation in the safety provided by buildings. Additionally, this analysis contributes to the literature addressing mass population displacement and provides new knowledge of organizational legitimacy in technical applications."
author_notes = []
authors = ["Miriam Hacker", "Jessica Kaminsky", "Kasey Faust", "Sebastien Rauch"]
date = ""
doi = "https://doi.org/10.1061/(ASCE)CO.1943-7862.0001820"
featured = false
projects = ["Disaster Migration and Civil Infrastructure: The Impacts of Sudden Population Influxes on Water and Sanitation Infrastructure"]
publication = "Journal of Construction Engineering and Management"
publication_short = "JCEM"
publication_types = ["2"]
publishDate = 2020-05-01T04:00:00Z
slides = ""
summary = ""
tags = ["Sweden", "Housing", "Legitimacy", "Forced displacement", "Regulation"]
title = "Regulatory Enforcement Approaches for Mass Population Displacement"
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
alt_text = "paved road through a row of uniform red and white single-story shelters"
caption = ""
focal_point = ""
preview_only = false

+++
