+++
abstract = ""
author_notes = []
authors = ["Miriam Hacker", "Christian Binz"]
date = 2021-02-01T05:00:00Z
doi = ""
featured = false
projects = ["Overcoming Institutional Barriers to Non-Grid Urban Water Systems (BARRIERS)"]
publication = ""
publication_short = ""
publication_types = ["3"]
publishDate = ""
slides = ""
summary = ""
tags = ["Water infrastructure", "Water reuse", "Science communication", "Transitions", "United States"]
title = "Summary Report: Institutional Complexity - Navigating diverse perspectives in the adoption of on-site water reuse"
url_code = ""
url_dataset = ""
url_pdf = "/uploads/2021_sf_institutionalcomplexity_summary-1.pdf"
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
