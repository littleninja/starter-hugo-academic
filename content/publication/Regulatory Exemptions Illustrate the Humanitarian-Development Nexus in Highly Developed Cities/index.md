+++
abstract = "In recent years, forcible displacement has increased around the globe, with significant numbers of people seeking shelter in urban areas. However, increased densification has added pressure to housing in these urban host communities, creating a situation where temporary accommodation is not always readily available. The integration of humanitarian response and pre-existing sustainable development activities is necessary to avoid disruptions to the provision of infrastructure services. This humanitarian-development nexus (HD-nexus) has proven to be difficult to operationalize. Using the experience of Sweden in 2015, this study looks at the provision of temporary accommodation for asylum-seekers within the existing regulatory framework as a place to explore the HD-nexus. Results show that humanitarian actors justify circumventing government institutions to achieve short-term response while development activities operate within these same institutions. Regulatory exemptions are one pathway by which we can observe this fundamental difference between the two approaches. Interviews with 19 individuals from government agencies, nonprofit organizations, and private companies were qualitatively analyzed to relate legitimacy with humanitarian response and development logics in the context of providing temporary accommodation. Results show that although formal regulatory definitions of temporary for temporary accommodations exist, this is not always adopted by stakeholders, leading to regulatory exemptions and non-compliance. Findings support decision-makers in improving response time and coordination for future events, and development goals of sustainable urban development."
author_notes = []
authors = ["Miriam Hacker", "Jessica Kaminsky", "Kasey Faust", "Sebastien Rauch"]
date = ""
doi = "https://doi.org/10.1016/j.ijdrr.2021.102309"
featured = false
projects = ["Disaster Migration and Civil Infrastructure: The Impacts of Sudden Population Influxes on Water and Sanitation Infrastructure"]
publication = "International Journal of Disaster Risk Reduction"
publication_short = "IJDRR"
publication_types = ["2"]
publishDate = 2021-07-01T04:00:00Z
slides = ""
summary = ""
tags = ["Legitimacy", "Housing", "Sweden", "Forced displacement", "Regulation"]
title = "Regulatory Exemptions Illustrate the Humanitarian-Development Nexus in Highly Developed Cities"
url_code = ""
url_dataset = ""
url_pdf = "/uploads/2021-regulatory-exemptions.pdf"
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
alt_text = "a long, flat building with living quarters on a clear day with a light snow on the grassy front yard"
caption = ""
focal_point = ""
preview_only = false

+++
