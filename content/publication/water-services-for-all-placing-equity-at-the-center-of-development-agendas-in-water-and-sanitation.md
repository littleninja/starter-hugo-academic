+++
abstract = "In an attempt to achieve Sustainable Development Goals (SDG) for water and sanitation, researchers, scientists, and industry professionals have developed alternative water sources to disrupt conventional centralized water and wastewater infrastructure. While a majority of work has been done in technology development, this study brings the conversation back to the role of equity in achieving SDGs, emphasizing the need for integrating equity in the adoption of new water and wastewater infrastructure. This paper provides an expanded framework for defining equity in alignment with sustainable development, allowing for other stakeholders to operationalize equity in their work. Although equity is applicable across the water and wastewater sector, we focus in on an emerging field where new types of technology systems are disrupting and supplementing the conventional provision of basic services: the case of onsite non-potable water systems in San Francisco, United States. Semi-structured interviews were conducted with stakeholders involved with on-site non-potable water reuse in San Francisco and the surrounding Bay area. Transcripts from the interviews are undergoing qualitative analysis using a deductive thematic coding approach, based on definitions for economic, environmental, and social equity. Initial results confirm a general sentiment: equity is necessary, but it is unclear how to incorporate in practice. In some cases, equity is seen as supplemental to utilities, but we propose that it actually needs to be explicitly incorporated, not only as a reaction to failure, but as a proactive approach to enhanced service provision . This study has implications for literature and practice, as it provides a more comprehensive definition for the various components within the equity discussion by categorizing forms of equity within economic, environmental, and social impact. Further, practitioners may use this work as foundational understanding in assessing internal operations and practices, improving infrastructure in the pathway to sustainable development."
author_notes = []
authors = ["Khalid Osman", "Miriam Hacker", "Kasey Faust", "Christian Binz"]
date = 2020-10-27T04:00:00Z
doi = ""
featured = false
projects = ["Overcoming Institutional Barriers to Non-Grid Urban Water Systems (BARRIERS)"]
publication = "Engineering Project Organization Conference"
publication_short = "EPOC"
publication_types = ["1"]
publishDate = ""
slides = ""
summary = ""
tags = ["United States", "Water reuse", "Water infrastructure", "Equity"]
title = "Water Services for All? Placing Equity at the Center of Development Agendas in Water and Sanitation"
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
If you would like a simplified version of this paper, a [summary report](/uploads/2021_sf-nyc_equity_summary.pdf) has been created.