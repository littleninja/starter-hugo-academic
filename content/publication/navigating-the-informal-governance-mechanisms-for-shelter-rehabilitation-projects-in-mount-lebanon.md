+++
abstract = "Lebanon has received displaced populations throughout its history, more recently, the country has experienced unprecedented increase of population. With over one quarter of its population representing displaced persons, predominantly from Syria, the country’s resources and infrastructure are under extreme pressure. With the position of the Lebanese Government to not allow permanent camps, humanitarian organizations have worked to find innovative solutions to improve shelter for vulnerable individuals in both the host community and displaced populations. This report focuses on shelter rehabilitation projects in the Mount Lebanon governorate. Specifically, twenty interviews conducted with partner organizations and municipality leadership regarding shelter activities, minimum standards, and coordination with stakeholders. Findings show the need for developing context-specific standards for shelter rehabilitation projects, coordination with local authorities during the implementation process, and the awareness that working within a framework without durable solutions leads to functioning within the parameters of a community’s informal governance system."
author_notes = []
authors = ["Miriam Hacker", "Jessica Kaminsky"]
date = 2019-02-22T05:00:00Z
doi = ""
featured = false
projects = ["Disaster Migration and Civil Infrastructure: The Impacts of Sudden Population Influxes on Water and Sanitation Infrastructure"]
publication = "USAID/OFDA, Habitat for Humanity International, InterAction"
publication_short = ""
publication_types = ["4"]
publishDate = ""
slides = ""
summary = ""
tags = ["Forced displacement", "Housing", "Governance", "Regulation", "Lebanon"]
title = "Navigating the Informal Governance Mechanisms for Shelter Rehabilitation Projects in Mount Lebanon"
url_code = ""
url_dataset = ""
url_pdf = "/uploads/20190212_hacker_minimumstandardsinlebanonurbanshelter_final.pdf"
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
