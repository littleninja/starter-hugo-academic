+++
abstract = "The implementation of housing regulations within crisis organization has received little attention in academic literature and yet plays a critical role in determining quality of living within accommodations and standard practice. During 2015, Germany experienced a rapid increase of displaced persons seeking asylum. Permanent reception centers and collective accommodations for people in the asylum application process quickly met capacity and additional space was required. To meet this need, government agencies coordinated with private companies and non-profit organizations to create alternative short and long-term housing solutions, such as renovated office spaces, schools, light-frame structures, and container housing. This study analyzes the role of regulations and contracts within temporary accommodations through the lens of housing managers and social workers. Six semi-structured interviews were conducted on-site with housing employees from non-profit organizations and private companies in one German city. These interviews were qualitatively coded, and excerpts relating to regulations and contracts for the facility were examined for emergent themes. Emergency housing guidelines have been introduced by various international organizations to maintain living conditions and safety for displaced persons, but primarily within the context of limited existing infrastructure. In contrast, this study explores the perception of housing regulations within the context of a more established built environment. Results from this study provide potential areas of continued research in the housing operation for government coordination including: (1) perceived difference of interaction with governmental agencies based on the type of accommodation (emergency vs. collective) and, (2) responsibilities of actors within crisis organization in relation to regulations and contracts. "
author_notes = []
authors = ["Miriam Hacker", "Jessica Kaminsky", "Kasey Faust"]
date = 2018-03-27T04:00:00Z
doi = ""
featured = false
projects = ["Disaster Migration and Civil Infrastructure: The Impacts of Sudden Population Influxes on Water and Sanitation Infrastructure"]
publication = "Construction Research Congress"
publication_short = "CRC"
publication_types = ["1"]
publishDate = ""
slides = ""
summary = ""
tags = ["Germany", "Forced displacement", "Housing", "Governance", "Regulation"]
title = "Housing Regulations in Temporary Accommodations for Displaced Persons: A German Case Study"
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
