+++
abstract = "Transitions from one socio-technical regime configuration to another entail long phases of institutional complexity, where two or more field logics co-exist in a sector and induce incompatibilities and frictions. This paper presents a dynamic phase model, which characterizes the types of institutional complexity that may build up and settle across various phases of a transition, illustrated with a case study from the diffusion of onsite water reuse in San Francisco. Results from semi-structured expert interviews and a focus group demonstrate that different forms of institutional complexity may follow each other in a transition trajectory and that formidable strategic agency is needed by the actors in a field in navigating prolonged phases of competing cultural demands. Gaining a more balanced perspective of both organizational and field-level reconfigurations may help better explain why transitions succeed in some places and fail in others."
author_notes = []
authors = ["Miriam Hacker", " Christian Binz"]
date = 2020-09-01T04:00:00Z
doi = "https://doi.org/10.1016/j.eist.2021.09.003"
featured = false
projects = ["Overcoming Institutional Barriers to Non-Grid Urban Water Systems (BARRIERS)"]
publication = "Environmental Innovation and Societal Transitions"
publication_short = "EIST"
publication_types = ["2"]
publishDate = ""
slides = ""
summary = ""
tags = ["Utilities", "Water infrastructure", "Water reuse", "Transitions", "United States"]
title = "Navigating Institutional Complexity in Socio-Technical Transitions"
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
alt_text = "stone planter filled with large-leafed green plants with understated flower spikes in front of an office windowfront"
caption = ""
focal_point = ""
preview_only = false

+++
If you would like a simplified version of this paper, a [summary report](/uploads/2021_sf_institutionalcomplexity_summary-1.pdf) has been created.