+++
abstract = "During temporary disruptions, synthetic organizations form to react quickly and accomplish a common goal. Germany had this type of disruption during the 2015 European refugee situation when existing temporary accommodations for displaced persons exceeded capacity and unconventional facilities, such as office spaces, warehouses and other commercial buildings, were used. Regulations do not necessarily maintain their role during these disruptions and this study visualizes the various types of regulatory-related interactions  that were experienced between stakeholders during the 2015 provision of temporary accommodations in four German cities. A total of 252 interactions were qualitatively coded for both buildings and either contracts or regulations  over 54 interviews with employees in government agencies, nonprofit organizations, utilities, and private companies. These excerpts were categorized by type of interaction, or whether an actor was constrained (98), neutral (65), or facilitated (89) , and by whom. Constrained interactions limited actors’ ability to engage in the temporary accommodation process, facilitated interactions allowed an actor to engage more freely with the process, and neutral interactions followed typical procedure. Results show that federal building regulations and the social affairs department constrain and facilitate interactions more often than stakeholders, supporting the theory that regulations can both empower and constrain within an organization.  Actors involved with government coordination, design, and facility management had high betweenness values, indicating a greater agency in the provision of temporary accommodations.  These findings provide insight to government agencies about how to expedite the work within a synthetic organization by targeting key, influential actors in the network. Additionally, this study highlights the need for understanding the perception of regulations amongst specific stakeholders to better explain why certain interactions were considered constrained, facilitated, or neutral. "
author_notes = []
authors = ["Miriam Hacker", "Jessica Kaminsky", "Kasey Faust"]
date = 2018-06-12T04:00:00Z
doi = ""
featured = false
projects = ["Disaster Migration and Civil Infrastructure: The Impacts of Sudden Population Influxes on Water and Sanitation Infrastructure"]
publication = "Engineering Project Organization Conference"
publication_short = "EPOC"
publication_types = ["1"]
publishDate = ""
slides = ""
summary = ""
tags = ["Social network analysis", "Forced displacement", "Housing", "Governance", "Regulation", "Germany"]
title = "Mapping Regulatory-Related Interactions of Stakeholders for Temporary Accommodations: a German Case Study"
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
