+++
abstract = "Scientists are increasingly exploring on-site water systems to supplement conventional centralized water and wastewater infrastructure. While major technological advancements have been achieved, we still lack a systematic view on the non-technical, or institutional, elements that constitute important barriers to the uptake of on-site urban water management systems. This paper presents a conceptual framework distinguishing between institutional barriers in six key dimensions: Equity, Knowledge and Capabilities, Financial Investment, Legal and Regulatory Frameworks, Legitimacy, and Market Structures. The analysis of the existing literature covering these barriers is translated into a typology of the socio-technical complexity of different types of alternative water systems (e.g., non-potable reuse, rainwater systems, and nutrient recovery). Findings show that socio-technical complexity increases with the pollution load in the source water, correlating to potential health risk, and the number of sectors involved in the value chain of an alternative water system. For example, greywater reuse for toilet flushing might have systematically less complex institutional barriers than source separation for agricultural reuse. This study provides practitioners with easily accessible means of understanding non-technical barriers for various types of on-site reuse systems and provides researchers with a conceptual framework for capturing socio-technical complexity in the adoption of alternative water systems."
author_notes = []
authors = ["Miriam Hacker", "Christian Binz"]
date = 2021-05-27T04:00:00Z
doi = "https://doi.org/10.1021/acs.est.0c07947"
featured = false
projects = ["Overcoming Institutional Barriers to Non-Grid Urban Water Systems (BARRIERS)"]
publication = "Environmental Science and Technology"
publication_short = "ES&T"
publication_types = ["2"]
publishDate = ""
slides = ""
summary = ""
tags = ["Literature review", "Water reuse", "Water infrastructure"]
title = "Institutional Barriers to On-Site Alternative Water Systems: A Conceptual Framework and Systematic Analysis of the Literature"
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
alt_text = "vertical rack of large white pipes with a number, yellow directional arrow, and control switches at the end of each"
caption = ""
focal_point = ""
preview_only = false

+++
If you would like a simplified version of this paper, a [summary report](/uploads/2021_barriersoverview_summary-3.pdf) has been created.