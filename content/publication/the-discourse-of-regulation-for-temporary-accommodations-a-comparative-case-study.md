+++
abstract = "The term regulation has been defined numerous ways in literature, yet it is unclear as to its function during times of extreme uncertainty, such as the provision of temporary accommodation in response to rapid population displacement. Critical discourse analysis is used to isolate and examine the ways in which regulation and its derivatives express power, cognitive understanding, and practice in relation to temporary accommodations for displaced persons in Germany, Sweden, and Lebanon. Since 2011 and peaking in 2015, these three countries represent the largest proportion of refugees and asylum-seekers per capita globally and in the European Union. This study includes 63 semi-structured interviews with individuals involved with providing or overseeing urban housing for displaced persons representing private companies, nonprofit organizations, and government agencies. Results show that while describing the temporary accommodation process, individuals express power through control of individuals (Power), normalization of the built environment (Cognitive Understanding), and versatility of regulations during times of uncertainty (Practice). These findings convey a unique portrait of many ways in which regulations function at the "
author_notes = []
authors = ["Miriam Hacker", "Julie Faure", "Kasey Faust", "Jessica Kaminsky"]
date = 2019-06-25T04:00:00Z
doi = ""
featured = false
projects = ["Disaster Migration and Civil Infrastructure: The Impacts of Sudden Population Influxes on Water and Sanitation Infrastructure"]
publication = "Engineering Project Organization Conference"
publication_short = "EPOC"
publication_types = ["1"]
publishDate = ""
slides = ""
summary = ""
tags = ["Governance", "Regulation", "Forced displacement", "Lebanon", "Sweden", "Germany"]
title = "The Discourse of Regulation for Temporary Accommodations: a Comparative Case Study"
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
