+++
abstract = ""
author_notes = []
authors = ["Miriam Hacker", "Christian Binz"]
date = 2020-02-26T05:00:00Z
doi = ""
featured = false
projects = ["Overcoming Institutional Barriers to Non-Grid Urban Water Systems (BARRIERS)"]
publication = ""
publication_short = ""
publication_types = ["4"]
publishDate = ""
slides = ""
summary = ""
tags = ["Water infrastructure", "Water reuse", "United States"]
title = "Focus Group Report: Understanding the enabling environment for on-site non-potable reuse"
url_code = ""
url_dataset = ""
url_pdf = "/uploads/eawag-sf-focusgroup_02-2020.pdf"
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
