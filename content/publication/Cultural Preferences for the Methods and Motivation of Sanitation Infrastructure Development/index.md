+++
abstract = "Research has found that sanitation infrastructure is cultured, or is shaped by national level cultural preferences. This study expands on this past work to identify causal pathways showing combinations of cultural dimensions that explain sanitation infrastructure technology choice, including total access to improved sanitation facilities, sewerage connections and access to onsite treatment technologies. This analysis uses fuzzy-set qualitative comparative analysis to analyze all possible combinations of causal conditions which contribute to an outcome of interest. In doing so, pathways are discovered using Hofstede's cultural dimensions as causal conditions and national-level sanitation data as outcomes. Findings show that the cultural dimensions of power distance, individualism versus collectivism, and uncertainty avoidance play a dominant role in sanitation technology choice. These cultural preferences are used to create an analytic framework that maps the cultural dimensions to the methods and motivations of common sanitation infrastructure delivery methods."
author_notes = []
authors = ["Miriam Hacker", "Jessica Kaminsky"]
date = 2017-07-24T04:00:00Z
doi = "https://doi.org/10.2166/washdev.2017.188"
external_link = ""
featured = false
links = []
projects = []
publication = "Journal of Water Sanitation and Hygiene for Development"
publication_short = "WashDev"
publication_types = ["2"]
publishDate = ""
slides = ""
summary = ""
tags = ["WASH", "Water infrastructure", "FsQCA"]
title = "Cultural Preferences for the Methods and Motivation of Sanitation Infrastructure Development"
url_code = ""
url_dataset = ""
url_pdf = "/uploads/2017-cultural-preferences.pdf"
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
alt_text = "sign labeled 'toilet' with an arrow pointing left, set in an forest understory of bright green ferns"
caption = ""
focal_point = ""
preview_only = false

+++
