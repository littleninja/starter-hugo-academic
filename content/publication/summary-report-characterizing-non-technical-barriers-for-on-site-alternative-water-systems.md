+++
abstract = ""
author_notes = []
authors = ["Miriam Hacker", "Christian Binz"]
date = 2021-03-01T05:00:00Z
doi = ""
featured = false
projects = ["Overcoming Institutional Barriers to Non-Grid Urban Water Systems (BARRIERS)"]
publication = ""
publication_short = ""
publication_types = ["3"]
publishDate = ""
slides = ""
summary = ""
tags = ["Water reuse", "Water infrastructure", "Science communication"]
title = "Summary Report: Characterizing non-technical barriers for on-site alternative water systems"
url_code = ""
url_dataset = ""
url_pdf = "/uploads/2021_barriersoverview_summary-3.pdf"
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
