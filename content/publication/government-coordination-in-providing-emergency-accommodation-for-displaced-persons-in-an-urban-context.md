+++
abstract = "In the summer of 2016, ten semi-structured interviews were conducted with government agencies in four German cities. Data from these interviews were qualitatively analyzed to discover the various approaches taken by each city’s government. The data describe participant’s perceived challenges, success and priorities involved with coordinating accommodations and present predominant themes within each of these areas, highlighting what needs to be improved, what worked well and what was prioritized throughout the process. Results from this study contribute critical insight to the limited research regarding construction practices and organizational design of infrastructure for emergency accommodation in an urban context. For example, the greatest challenge expressed by government officials were associated with the process of finding available housing and preparing facilities. External coordination with the community and private companies was identified as one of the predominant successes of the housing operation. Lastly, different aspects of the preparation of facilities was expressed as the biggest priority, such as minimizing costs, finding available properties and providing utility upgrades. Describing governments’ organizational structures along with their benefits and drawbacks provides other government agencies with foresight in emergency planning and crisis response techniques, and contributes to increased infrastructure resilience in future emergency response."
author_notes = []
authors = ["Miriam Hacker", "Jessica Kaminsky", "Kasey Faust"]
date = 2017-05-31T04:00:00Z
doi = ""
featured = false
projects = ["Disaster Migration and Civil Infrastructure: The Impacts of Sudden Population Influxes on Water and Sanitation Infrastructure"]
publication = "Construction Research Congress"
publication_short = "CSCE / CRC"
publication_types = ["1"]
publishDate = ""
slides = ""
summary = ""
tags = ["Germany", "Governance", "Housing", "Forced displacement"]
title = "Government Coordination in Providing Emergency Accommodation for Displaced Persons in an Urban Context"
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
