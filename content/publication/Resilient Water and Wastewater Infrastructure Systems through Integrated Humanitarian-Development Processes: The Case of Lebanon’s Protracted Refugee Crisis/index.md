+++
abstract = "When populations are displaced, say after a hurricane or a man-made crisis, water and wastewater utilities can face a real challenge in providing services to those displaced. The challenge is especially difficult when the local infrastructure was already strained in trying to meet the host community’s pre-displacement demand. What most communities need are resilient water and wastewater infrastructure systems, and what we develop in this paper is an integrated approach that can achieve such systems. Our approach takes into account the operating environment of bridging what some call the humanitarian-development (HD) nexus. The HD nexus is the phase in which a community transitions toward a response paradigm that combines humanitarian response with long-term services. The HD nexus poses inherent contextual challenges, and we identify them, through interviews with municipalities in Lebanon, in their physical, social, financial, and institutional dimensions. Furthermore, we explore interactions that can inform how best to address these challenges. Our results introduce policy areas (i.e., utility pricing and establishing shared development priorities) that support this transition across the HD nexus and achieve resilient systems. Our discussions give rise to an empirical understanding of the infrastructures’ operating environments and thus contribute to global conversations on sustainable development."
author_notes = []
authors = ["Amal Bakchan", "Miriam Hacker", "Kasey Faust"]
date = ""
doi = "https://doi.org/10.1021/acs.est.0c05630"
external_link = ""
featured = false
links = []
projects = ["Disaster Migration and Civil Infrastructure: The Impacts of Sudden Population Influxes on Water and Sanitation Infrastructure"]
publication = "Environmental Science and Technology"
publication_short = "ES&T"
publication_types = ["2"]
publishDate = 2021-03-12T05:00:00Z
slides = ""
summary = ""
tags = ["Lebanon", "Water infrastructure", "Forced displacement"]
title = "Resilient Water and Wastewater Infrastructure Systems through Integrated Humanitarian-Development Processes: The Case of Lebanon’s Protracted Refugee Crisis."
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
alt_text = "Lebanese apartment building on a clear day, the bottom wall is painted with graffiti art of a girl with pink-purple hair with lavender eyes staring absently"
caption = ""
focal_point = ""
preview_only = false

+++
