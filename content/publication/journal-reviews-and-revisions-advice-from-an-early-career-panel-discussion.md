+++
abstract = ""
author_notes = []
authors = ["Paul Chan", "Carrie Dossick", "Miriam Hacker", "Timo Hartmann", "Amy Javernick-Will", "Ashwin Mahalingam", "Vedran Zerjav"]
date = 2020-01-01T05:00:00Z
doi = "https://doi.org/10.25219/epoj.2020.00101"
featured = false
projects = []
publication = "Engineering Project Organization Journal"
publication_short = "EPOJ"
publication_types = ["3"]
publishDate = ""
slides = ""
summary = ""
tags = ["Early career", "Academia", "Publishing"]
title = "Journal Reviews and Revisions: Advice from an Early Career Panel Discussion"
url_code = ""
url_dataset = ""
url_pdf = "/uploads/epoj-2020.pdf"
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
