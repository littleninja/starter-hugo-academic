+++
abstract = ""
author_notes = []
authors = ["Miriam Hacker", "Christian Binz", "Khalid Osman", "Kasey Faust"]
date = 2021-04-01T04:00:00Z
doi = ""
featured = false
projects = ["Overcoming Institutional Barriers to Non-Grid Urban Water Systems (BARRIERS)"]
publication = ""
publication_short = ""
publication_types = ["3"]
publishDate = ""
slides = ""
summary = ""
tags = ["United States", "Equity", "Water reuse", "Water infrastructure", "Science communication"]
title = "Summary Report: Developing a Path for Equity in Emerging water and Wastewater Infrastructure"
url_code = ""
url_dataset = ""
url_pdf = "/uploads/2021_sf-nyc_equity_summary.pdf"
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
