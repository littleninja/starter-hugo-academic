+++
abstract = "In 2015, 28 European countries cumulatively received over two million applications for asylum, almost three times more than in the previous year. This resulted in pre-existing accommodation facilities reaching capacity and requiring the provision of urban emergency accommodations in unconventional buildings. To meet this housing need, ad hoc task forces across multiple disciplines formed to mitigate the extreme uncertainty of providing infrastructure services in a short period of time. The involvement of water and wastewater utilities in this technical project was explored through employee perspectives from two German water and wastewater utilities using qualitative analysis techniques. Ethnographic interviews were iteratively coded for excerpts legitimizing the interviewee’s involvement in providing water or wastewater services for emergency accommodations. Results show three emergent themes from utility employees: the necessity of improvisation during the design process, confidence in the situational response by individuals and the utility, and the necessity for improved coordination with other actors in the synthetic organization. In addition, this work provides a theoretical framework for the technical application of organizational legitimacy theory in the circumstances of extreme contextual uncertainty. Practical implications of this work suggest utility monitoring of emergency accommodations for improved design and better protocols for coordinating with other actors."
author_notes = []
authors = ["Miriam Hacker", "Jessica Kaminsky", "Kasey Faust"]
date = 2019-04-01T04:00:00Z
doi = "https://doi.org/10.1061/(ASCE)CO.1943-7862.0001622"
featured = false
projects = ["Disaster Migration and Civil Infrastructure: The Impacts of Sudden Population Influxes on Water and Sanitation Infrastructure"]
publication = "Journal of Construction Engineering and Management"
publication_short = "JCEM"
publication_types = ["2"]
publishDate = ""
slides = ""
summary = ""
tags = ["legitimacy", "germany", "forced displacement", "water infrastructure", "utilities"]
title = "Legitimizing Involvement in Emergency Accommodations: Water and Wastewater Utility Perspectives"
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
alt_text = "stone building wall next to a covered archway, the wall has 'REFUGEES WELCOME' graffitied on it"
caption = ""
focal_point = ""
preview_only = false

+++
