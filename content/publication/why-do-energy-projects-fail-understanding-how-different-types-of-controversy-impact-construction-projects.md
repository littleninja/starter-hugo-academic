+++
abstract = "The successful implementation and operation of energy projects is critical to meet the world’s increasing energy demands. These infrastructure projects often face challenges due to opposition from various external stakeholders. Conflict between stakeholders has the potential to cause delays, cost overruns, and even project termination. It is critical that developers anticipate potential controversy when planning projects; yet limited research exists as a point of reference. We seek to fill this gap by collecting and analyzing data about previous energy projects to identify stakeholders, types of controversy, and resulting consequences. Specifically, we perform a media analysis of news articles concerning wind energy projects globally. Results indicate that community members are the most active stakeholders involved in efforts opposing an energy project. Most stakeholders are especially active during the project’s proposal phase, often participating in public meetings and commenting periods. Community outreach may mitigate negative consequences, demonstrating the importance for developers to engage and learn about the community’s concerns early in the process. By anticipating potential controversy, developers will be able to plan for cost and schedule impacts and mitigate unforeseen consequences. They may be able to engage with community members to reduce opposition or include additional funds and days in the project schedule to account for inevitable setbacks. Government agencies may also find this useful when introducing strategic requirements in the regulatory process to improve community engagement and minimize escalation of controversy. Ultimately, a better understanding of the impacts of controversy will lead to more efficient project construction."
author_notes = []
authors = ["Michaela LaPatin", "Lauryn Spearing", "Helena Tiedmann", "Olga Kavvada", "Maria Giorda", "Jean Daniélou", "Miriam Hacker", "Kasey Faust"]
date = 2021-05-27T04:00:00Z
doi = ""
featured = false
projects = []
publication = " Annual Conference of the Canadian Society for Civil Engineering"
publication_short = "CSCE"
publication_types = ["1"]
publishDate = ""
slides = ""
summary = ""
tags = ["Transitions", "Community engagement", "Controversy", "Renewable energy"]
title = "Why do energy projects fail? Understanding how different types of controversy impact construction projects"
url_code = ""
url_dataset = ""
url_pdf = ""
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[image]
caption = ""
focal_point = ""
preview_only = false

+++
