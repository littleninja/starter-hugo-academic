---
widget: portfolio
headless: true
weight: "65"
title: Projects
subtitle: ''
content:
  page_type: project
  filter_default: "0"
  filter_button:
  - name: All
    tag: "*"
  - name: Water Reuse
    tag: "Water reuse"
  - name: Forced displacement
    tag: "Forced displacement"
  - name: Transitions
    tag: "Transitions"
  - name: Science Communication
    tag: "Science communication"
design:
  columns: "2"
  view: 2
  flip_alt_rows: false

---
