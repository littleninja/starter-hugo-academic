---
# An instance of the Featurette widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: featurette

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 30

title: Types of analysis
subtitle:

# Showcase personal skills or business features.
# - Add/remove as many `feature` blocks below as you like.
# - For available icons, see: https://wowchemy.com/docs/page-builder/#icons
feature:
- name: Systematic literature reviews
  icon: book-reader
  icon_pack: fas
- name: Thematic coding
  icon: sitemap
  icon_pack: fas
- name: Sentiment analysis
  icon: theater-masks
  icon_pack: fas
- name: Media analysis
  icon: photo-video
  icon_pack: fas
- name: Social network analysis
  icon: people-arrows
  icon_pack: fas
- name: Other qualitative methods
  icon: clipboard-list
  icon_pack: fas

# Uncomment to use emoji icons.
#- icon: ":smile:"
#  icon_pack: "emoji"
#  name: "Emojiness"
#  description: "100%"  

# Uncomment to use custom SVG icons.
# Place your custom SVG icon in `assets/media/icons/`.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
# For example, reference `assets/media/icons/xyz.svg` as `icon: 'xyz'`
#- icon: "your-custom-icon-name"
#  icon_pack: "custom"
#  name: "Surfing"
#  description: "90%"
---
