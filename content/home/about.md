---
widget: about
active: true
headless: true
weight: 10
title: Biography
author: admin

design:
  columns: "1"
  background:
    image: background-about.jpg
    image_darken: 1
    image_parallax: true
    image_position: center
    image_size: cover
    text_color_light: false
  # spacing:
  #   padding: ["1.6rem", "0", "2rem", "0"]

---
