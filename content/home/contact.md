---
widget: contact
headless: true
weight: "130"
title: Contact
subtitle: 
content:
  # Automatically link email and phone or display as text?
  autolink: false
  
  # Email form provider
  # form:
  #   provider: netlify
  #   formspree:
  #     id:
  #   netlify:
  #     # Enable CAPTCHA challenge to reduce spam?
  #     captcha: false
  
design:
  columns: "2"
  background:
    image: background-footer.jpg
    image_darken: 1
    image_parallax: false
    image_position: center
    image_size: cover
    text_color_light: false

---
