+++
date = "2019-2021"
external_link = ""
links = []
slides = ""
summary = "(2019 - 2021)"
tags = ["Governance", "Water reuse", "Transitions"]
title = "Overcoming Institutional Barriers to Non-Grid Urban Water Systems (BARRIERS)"
url_code = ""
url_pdf = ""
url_slides = ""
url_video = ""
[image]
alt_text = "a short stack of purple PVC pipes"
caption = ""
focal_point = ""
preview_only = false

+++
## 2019 - 2021

_Postdoctoral researcher_

### Funding

* Swiss Federal Institute of Aquatic Science and Technology (Eawag) discretionary funds

### Description

BARRIERS is a trans-disciplinary project with the Swiss Federal Institute for Aquatic Science & Technology (Eawag) that is focused on systematizing the regulatory, social, and cultural barriers encountered for onsite urban water management (UWM) technologies. By identifying these barriers across actor groups, technology types, and economic contexts in San Francisco and New York City, other cities looking to implement innovative onsite UWM technology will be better equipped to understand and circumvent these challenges within their operating environments.

Stakeholder engagement is a critical component to this work. We brought together practitioners that participated in the interviews, as well as others that could benefit for two city-specific workshops to discuss initial findings. Additionally, all published academic articles have been put into shortened summary reports (see publications) for ease of accessibility.

### Project website

[https://www.eawag.ch/en/department/ess/projects/cirus-barriers-to-on-site-uwm/](https://www.eawag.ch/en/department/ess/projects/cirus-barriers-to-on-site-uwm/ "Official project site (Eawag).")