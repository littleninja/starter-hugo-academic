+++
date = "2015-2018"
external_link = ""
links = []
slides = ""
summary = "(2015 - 2018)"
tags = ["Housing", "Governance", "Water infrastructure", "Forced displacement"]
title = "Disaster Migration and Civil Infrastructure: The Impacts of Sudden Population Influxes on Water and Sanitation Infrastructure"
url_code = ""
url_pdf = ""
url_slides = ""
url_video = ""
[image]
alt_text = "shiny black canister vandalized with a few stickers, one reading 'Refugees Welcome' with a yellow outline of two parents and child running away"
focal_point = ""
preview_only = false

+++
## 2015 - 2018

_Doctoral researcher_

### Funding

* National Science Foundation (NSF) (Grant No. 1624409 and 1624417)
* Valle Scholarship and Scandinavian Exchange Program
* United States Agency for International Development Office of US Foreign Disaster Assistance (USAID/OFDA) and Habitat for Humanity International (HHI) Graduate Student Fellowship for Humanitarian Shelter and Settlements

### Description

Increased political, social, and climate-related events have resulted in more forcibly displaced populations than at the end of World War II. Quite a bit of literature has looked at disaster response in affected communities, but there is a need to better understand how host communities (especially those in urban spaces) accommodate those displaced. This project originally intended to focus on impacts to water and wastewater networks from setting up temporary accommodations in non-residential buildings. Quickly we found that data was not being collected in a way to quantitatively analyze the situation. However, there was a need to understand the institutional response to the situation, recognizing that coordination, guidelines, and regulatory oversight to the process has implications for those being housed, infrastructure systems, and long-term impacts to the built environment.

This work is focused in Germany, Sweden, and Lebanon.