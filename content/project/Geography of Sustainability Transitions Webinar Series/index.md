+++
date = "2020"
external_link = ""
links = []
slides = ""
summary = "(2020) "
tags = ["Sustainability", "Science communication", "Transitions"]
title = "Geography of Sustainability Transitions Webinar Series"
url_code = ""
url_pdf = ""
url_slides = ""
url_video = ""
[image]
alt_text = ""
caption = ""
focal_point = ""
preview_only = false

+++
## 2020

_Social media coordinator_

### Description

The geography of sustainability transitions (GeoST) is a quickly evolving research theme at the interface of transition studies and human geography. This online event explores the current state of the art and identifies promising future research themes that expand ongoing dialogues between the transitions and geography communities.

To support this effort, created external-facing communication materials for the group and webinar series which others have continued to build.

Such materials include:

* Managing tweets through the host account: @Cirus_Eawag
* Developing an [event website](https://geographyoftransitions.wordpress.com/), which has since evolved into a thematic group. Note: The theme is consistent from its original design.
* Overview document for the event ![](/uploads/geost-webinar-series-save-the-date-1.png)
* Twitter posts for each event ![](/uploads/webinar-intro.png)
* As well as joint advertisements for additional events in the Sustainability Transitions Research Network (STRN) ![](/uploads/geost-strn-webinar-day-nov-4.png)