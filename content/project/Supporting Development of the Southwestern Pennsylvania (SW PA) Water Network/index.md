+++
date = "2021 - Present"
external_link = ""
links = []
slides = ""
summary = "(2021 - Present)"
tags = ["Water infrastructure", "Governance", "Networks", "Science communication"]
title = "Supporting Development of the Southwestern Pennsylvania (SW PA) Water Network"
url_code = ""
url_pdf = ""
url_slides = ""
url_video = ""
[image]
alt_text = ""
caption = ""
focal_point = ""
preview_only = false

+++
## 2021 - Present

_Project lead / Network liaison_

### Funding

* Heinz Endowments

### Description

Since 2018, the Water Center at Penn has been involved with investigating the challenges and opportunities for effective and continued water management in the Southwestern Pennsylvania (SW PA) region. Building on previous progress made, the SW PA Water Network was established and work is currently being done to implement recommendations, specifically the selection of a host organization and a local coordinator, and the transition process. Current work also builds on the areas of high energy for specific water issues as expressed by network members, facilitating opportunities to connect and participate in the network and finding potential joint collaboration.

My work involves supporting meetings, identifying themes and structuring conversations in a way that best centers local water leaders and the Network's vision. As a by-product, I've developed a number of communication materials, including:

* SW PA Water Network - [Overview Document](https://app.forestry.io/sites/6awsmjuymsbmfg/body-media//uploads/sw-pa-water-network-overview-aug-2021-3.pdf)
* SW PA Leadership Incubator - [Program Overview](https://app.forestry.io/sites/6awsmjuymsbmfg/body-media//uploads/leadership-incubator-call-for-applications-2021-2022-8-1.pdf)
* Logo for the network ![](/uploads/swpa-logo.jpg)
* Meeting materials ![](/uploads/information-session.png)
* Network announcements

  ![](/uploads/sw-pa-host-org-selection-process-timeline.png)

### Project website

[https://watercenter.sas.upenn.edu/three-rivers-watershed-action-network-and-leadership-incubator/](https://watercenter.sas.upenn.edu/three-rivers-watershed-action-network-and-leadership-incubator/ "https://watercenter.sas.upenn.edu/three-rivers-watershed-action-network-and-leadership-incubator/")