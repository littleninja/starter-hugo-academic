---
title: Miriam Hacker
superuser: false
role: Research Program Manager
organizations:
- name: The Water Research Foundation
  url: https://www.waterrf.org/
bio: ''
interests:
- Regulation
- Water Governance
- Water Reuse
- Organizational Legitimacy
- Institutional Theory
education:
  courses:
  - course: PhD - Civil and Environmental Engineering
    institution: University of Washington
    year: "2018"
  - course: MS - Civil and Environmental Engineering
    institution: University of Washington
    year: "2014"
  - course: BS - Civil and Environmental Engineering
    institution: University of Washington
    year: "2011"
social:
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=5f22OzsAAAAJ&hl=en
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/miriamhacker1
- icon: researchgate
  icon_pack: ai
  link: https://www.researchgate.net/profile/Miriam-Hacker-4?ev=hdr_xprf
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/miriam-hacker/
- icon: envelope
  icon_pack: fas
  link: "/#contact"
email: hackermiriam@gmail.com
highlight_name: true

---
Originally from the Pacific Northwest, Miriam Hacker currently works as a Research Program Manager at The Water Research Foundation. Her background is in civil engineering from the University of Washington and she has a passion for understanding social implications of water and wastewater infrastructure. Her professional experience includes stormwater permitting for residential development, and research focusing on the socio-technical barriers for adoption of on-site water reuse with the Swiss Federal Institute for Aquatic Science and Technology. 

While much work has been done to develop new and emerging technology, Miriam is driven by the desire to understand the external stressors motivating such technology development, and the enabling environment in which such technology is introduced.

When not immersed in research, Miriam attempts to keep houseplants alive, loves to explore local coffee shops and bookstores, and making friends with puppies.

{{< icon name="download" pack="fas" >}} Download my {{< staticref "/uploads/cv_2021_miriamhacker.pdf" "newtab" >}}CV{{< /staticref >}}.